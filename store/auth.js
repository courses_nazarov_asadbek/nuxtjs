export const state = () => ({
  token: null,
})
export const mutations = {
  setToken(state, payload) {
    state.token = payload
  },
  clearToken(state) {
    state.token = null
  }
}
export const actions = {
  login({commit}) {
    try {
      commit('setToken', 'true')
    } catch (e) {
      throw e
    }
  },
  logout({commit}) {
    try {
      commit('clearToken')
    } catch (e) {
      throw e
    }
  },
}
export const getters = {
  isAuth: state => !!state.token
}
