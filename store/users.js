export const state = () => ({
  users: [],
  user: []
})
export const mutations = {
  setUsers(state, payload) {
    state.users = payload
  }
}
export const actions = {
  async fetchUsers({commit}) {
    try {
      const users = await this.$axios.$get('https://jsonplaceholder.typicode.com/users')
      commit('setUsers', users)
    } catch (e) {
      throw e
    }
  },
  async fetchUserById({}, userId) {
    try {
      return await this.$axios.$get(`https://jsonplaceholder.typicode.com/users/${userId}`)
    } catch (e) {
      throw e
    }
  },
}
export const getters = {
  GET_USERS: state => state.users
}
